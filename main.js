$(document).ready(function () {
    $(`.tabs-caption li`).click(function () {
        switchTabs(this, `active`, `tabs-content`);
    });

    function switchTabs(elem, switchClass, contentClass) {
        $(elem).addClass(switchClass).siblings().removeClass(switchClass);
        const tabIndex = $(elem).index();
        $(`.${contentClass}`).removeClass(switchClass).eq(tabIndex).addClass(switchClass);
    }

});
//=======================================================
$(document).ready(function () {

    $(".item").hide();
    $(".item").slice(0, 8).show();
    $(".btn-load-more").click(function () {
        const imageType = $(".btn.active").data("filter");
        $(`.item${imageType}:hidden`).slice(0, 8).show();
    });

    $('.btn').click(function () {
        $(this).addClass('active').siblings().removeClass("active");
        const imageType = $(this).data("filter");
        $(".item").hide();

        $(`.item${imageType}`).slice(0, 8).show();
    });
});
//============================================================
$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    focusOnSelect: true
});